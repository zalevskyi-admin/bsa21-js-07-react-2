import { Action, ActionType, Message } from './rootReducer';

export function loadStart(): Action {
  return { type: ActionType.loadStart };
}
export function loadMessages(messages: Message[]): Action {
  return { type: ActionType.loadMessages, payload: { messages } };
}
export function setLoadError(error: string): Action {
  return { type: ActionType.setLoadError, payload: { error } };
}
export function addMessage(text: string): Action {
  return { type: ActionType.addMessage, payload: { text } };
}
export function deleteMessage(id: string): Action {
  return { type: ActionType.deleteMessage, payload: { id } };
}
export function setMessageEdit(id: string): Action {
  return { type: ActionType.setMessageEdit, payload: { id } };
}
export function cancelMessageEdit(): Action {
  return { type: ActionType.cancelMessageEdit };
}
export function updateText(id: string, text: string): Action {
  return { type: ActionType.updateText, payload: { id, text } };
}
export function toggleLike(id: string): Action {
  return { type: ActionType.toggleLike, payload: { id } };
}
