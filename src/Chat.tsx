import React from 'react';
import { Preloader } from './components/Preloader';
import LoadError from './components/LoadError';
import Header from './components/Header';
import './Chat.css';
import MessageList from './components/MessageList';
import { MessageInputSend } from './components/MessageInputSend';
import { Action, Message, Chat as ChatStateProps, State } from './rootReducer';
import { connect } from 'react-redux';
import * as actions from './actions';
import { ModalEdit } from './components/ModalEdit';

interface DispatchProps {
  loadMessages: (messages: Message[]) => Action;
  setLoadError: (error: string) => Action;
  updateText: (id: string, text: string) => Action;
  addMessage: (text: string) => Action;
  cancelMessageEdit: () => Action;
}

interface OwnProps {
  url: string;
}

type Props = ChatStateProps & DispatchProps & OwnProps;

export class Chat extends React.Component<Props> {
  render() {
    return (
      <div className='chat'>
        {this.props.preloader ? (
          <Preloader />
        ) : this.props.loadError ? (
          <LoadError />
        ) : (
          <>
            <Header />
            <MessageList />
            <MessageInputSend send={this.props.addMessage} />
            {this.props.messageToEditId && this.props.messageToEditText ? (
              <ModalEdit
                id={this.props.messageToEditId}
                text={this.props.messageToEditText}
                update={this.props.updateText}
                cancel={this.props.cancelMessageEdit}
              />
            ) : null}
          </>
        )}
      </div>
    );
  }
  componentDidMount() {
    this.loadData();
  }
  async loadData() {
    const response = await fetch(this.props.url);
    if (!response.ok) {
      console.log('loading error');
      this.props.setLoadError(`Loadding error: ${response.status}`);
    } else {
      const responseJSON = await response.json();
      responseJSON.sort(sortByCreatedDate);
      this.props.loadMessages(responseJSON);
    }
  }
}

function sortByCreatedDate(a: Message, b: Message) {
  if (a.createdAt > b.createdAt) {
    return 1;
  } else if (a.createdAt < b.createdAt) {
    return -1;
  }
  return 0;
}

const mapStateToProps = (state: State, ownProps: OwnProps) => {
  const chat = state.chat;
  return { ...chat };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
