import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Chat from './Chat';
import { Provider } from 'react-redux';
import store from './store';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Chat url='https://edikdolynskyi.github.io/react_sources/messages.json' />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
