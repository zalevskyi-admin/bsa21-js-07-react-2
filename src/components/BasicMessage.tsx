import React from 'react';
import '../styles/BasicMessage.css';

export class BasicMessage extends React.Component<{
  text: string;
  time: string;
}> {
  render() {
    return (
      <>
        <Text text={this.props.text} />
        <Time time={this.props.time} />
      </>
    );
  }
}

class Text extends React.Component<{ text: string }> {
  render() {
    return <div className='message-text'>{this.props.text}</div>;
  }
}

class Time extends React.Component<{ time: string }> {
  render() {
    return <div className='message-time'>{this.props.time}</div>;
  }
}
