import React from 'react';
import '../styles/ModalEdit.css';

interface Props {
  id: string;
  text: string;
  update: Function;
  cancel: Function;
}

export class ModalEdit extends React.Component<Props> {
  state: {
    text: string;
  };
  constructor(props: Props) {
    super(props);
    this.state = { text: this.props.text };
  }
  render() {
    return (
      <div className='edit-message-modal modal-shown'>
        <div className='modal-content'>
          <textarea
            className='edit-message-input'
            value={this.state.text}
            onChange={e => this.setText(e.target.value)}></textarea>

          <button
            className='edit-message-close'
            onClick={e => this.props.cancel()}>
            Cancel
          </button>
          <button
            className='edit-message-button'
            onClick={e => this.props.update(this.props.id, this.state.text)}>
            Save
          </button>
        </div>
      </div>
    );
  }
  setText(text: string) {
    this.setState({ text: text });
  }
}
