import React from 'react';
import { connect } from 'react-redux';
import { State } from '../rootReducer';
import '../styles/LoadError.css';

class LoadError extends React.Component<{ loadError: string | null }> {
  render() {
    return <div className='load-error'>{this.props.loadError}</div>;
  }
}

const mapStateToProps = (state: State) => ({
  loadError: state.chat.loadError,
});

export default connect(mapStateToProps)(LoadError);
