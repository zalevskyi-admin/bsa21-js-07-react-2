import React from 'react';
import '../styles/MessageInput.css';

interface MessageInputProps {
  id: string;
  text: string;
  update: Function;
}

export class MessageInputUpdate extends React.Component<MessageInputProps> {
  state: {
    text: string;
  };
  constructor(props: MessageInputProps) {
    super(props);
    this.state = { text: this.props.text };
  }
  render() {
    return (
      <div className='message-input'>
        <textarea
          className='message-input-text'
          value={this.state.text}
          onChange={e => this.setText(e.target.value)}></textarea>
        <button
          className='message-input-button'
          onClick={e => this.props.update(this.props.id, this.state.text)}>
          Edit
        </button>
      </div>
    );
  }
  onChange(key: string, value: string) {
    this.setState({ [key]: value });
  }
  setText(text: string) {
    this.setState({ text: text });
  }
}
