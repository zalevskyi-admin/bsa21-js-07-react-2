import React from 'react';
import '../styles/MessageList.css';
import { Action, Message as MessageData, State } from '../rootReducer';
import { Message } from './Message';
import { OwnMessage } from './OwnMessage';
import { connect } from 'react-redux';
import * as actions from '../actions';

export class MessageList extends React.Component<{
  currentUserId: string;
  messages: MessageData[];
  toggleLike: (id: string) => Action;
  deleteMessage: (id: string) => Action;
  setMessageEdit: (id: string, text: string) => Action;
}> {
  render() {
    const messagesByDate = splitByDates(this.props.messages);
    return (
      <div className='message-list'>
        {messagesByDate.map(date => {
          const [dateValue, dateMessages] = date;
          return (
            <MessageListFragment
              currentUserId={this.props.currentUserId}
              date={dateValue}
              messages={dateMessages}
              toggleLike={this.props.toggleLike}
              edit={this.props.setMessageEdit}
              delete={this.props.deleteMessage}
              key={dateValue}
            />
          );
        })}
      </div>
    );
  }
}

class MessageDivider extends React.Component<{ date: string }> {
  render() {
    return (
      <div className='messages-divider' key={this.props.date}>
        {this.props.date}
      </div>
    );
  }
}

class MessageListFragment extends React.Component<{
  currentUserId: string;
  date: string;
  messages: MessageData[];
  toggleLike: Function;
  edit: Function;
  delete: Function;
}> {
  render() {
    return (
      <>
        <MessageDivider date={this.props.date} />
        {this.props.messages.map(message =>
          message.userId === this.props.currentUserId ? (
            <OwnMessage
              key={message.id}
              message={message}
              edit={this.props.edit}
              delete={this.props.delete}
            />
          ) : (
            <Message
              key={message.id}
              message={message}
              toggleLike={this.props.toggleLike}
            />
          ),
        )}
      </>
    );
  }
}

function splitByDates(messages: MessageData[]): Array<[string, MessageData[]]> {
  const messagesByDate: Map<string, MessageData[]> = new Map();
  const result: Array<[string, MessageData[]]> = [];
  messages.forEach(message => {
    const dateFormated = bsaDateFormat(message.createdAt);
    if (messagesByDate.has(dateFormated)) {
      const arr = messagesByDate.get(dateFormated) as MessageData[];
      arr.push(message);
      messagesByDate.set(dateFormated, arr);
    } else {
      messagesByDate.set(dateFormated, [message]);
    }
  });
  messagesByDate.forEach((messages, date) => {
    result.push([date, messages]);
  });
  return result;
}

function bsaDateFormat(isoDate: string): string {
  const today = new Date();
  const yesterday = new Date(
    today.getFullYear(),
    today.getMonth(),
    today.getDate() - 1,
  );
  const todayFormated = new Intl.DateTimeFormat('en-US', {
    dateStyle: 'full',
  }).format(today);
  const yesterdayFormated = new Intl.DateTimeFormat('en-US', {
    dateStyle: 'full',
  }).format(yesterday);
  const dateFormated = new Intl.DateTimeFormat('en-US', {
    dateStyle: 'full',
  }).format(new Date(isoDate));
  if (dateFormated === todayFormated) {
    return 'Today';
  }
  if (dateFormated === yesterdayFormated) {
    return 'Yesterday';
  }
  const [day, monthDate] = dateFormated.split(', ');
  const [month, date] = monthDate.split(' ');
  return `${day}, ${date} ${month}`;
}

const mapStateToProps = (state: State) => {
  const chat = state.chat;
  return { ...chat };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
