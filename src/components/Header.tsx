import React from 'react';
import { connect } from 'react-redux';
import { Message, State } from '../rootReducer';
import '../styles/Header.css';

class Header extends React.Component<{
  title: string;
  messages: Message[];
}> {
  render() {
    return (
      <div className='header'>
        <Title title={this.props.title} />
        <UsersCount count={usersCount(this.props.messages)} />
        <MessagesCount count={this.props.messages.length} />
        <LastMessageDate
          lastDate={
            this.props.messages.length
              ? this.props.messages[this.props.messages.length - 1].createdAt
              : null
          }
        />
      </div>
    );
  }
}

class Title extends React.Component<{ title: string }> {
  render() {
    return <div className='header-title'>{this.props.title}</div>;
  }
}

class UsersCount extends React.Component<{ count: number }> {
  render() {
    return (
      <div className='header-users-count-wrapper'>
        <span className='header-users-count'>{this.props.count}</span>
        <span> User{this.props.count !== 1 && 's'}</span>
      </div>
    );
  }
}

class MessagesCount extends React.Component<{ count: number }> {
  render() {
    return (
      <div className='header-messages-count-wrapper'>
        <span className='header-messages-count'>{this.props.count}</span>
        <span> Message{this.props.count !== 1 && 's'}</span>
      </div>
    );
  }
}

class LastMessageDate extends React.Component<{ lastDate: string | null }> {
  render() {
    return (
      <div className='header-last-message-date-wrapper'>
        {this.props.lastDate ? (
          <>
            <span>Last message at: </span>
            <span className='header-last-message-date'>
              {formatDate(this.props.lastDate)}
            </span>
          </>
        ) : (
          <>
            <span>No messages yet</span>
            <span className='header-last-message-date'></span>
          </>
        )}
      </div>
    );
  }
}

function usersCount(messages: Message[]) {
  const uniqueUsers = new Set();
  messages.forEach(message => {
    if (!uniqueUsers.has(message.userId)) {
      uniqueUsers.add(message.userId);
    }
  });
  return uniqueUsers.size;
}

function formatDate(isoDate: string): string {
  // ISO format (ISO 8601), can be 24-27 characters long
  // YYYY-MM-DDTHH:mm:ss.sssZ or ±YYYYYY-MM-DDTHH:mm:ss.sssZ
  const [datePart, timePart] = isoDate.split('T');
  const ISO_10_CHAR = 10;
  const char_shift = datePart.length - ISO_10_CHAR;
  const year = datePart.slice(0 + char_shift, 4 + char_shift);
  const month = datePart.slice(5 + char_shift, 7 + char_shift);
  const day = datePart.slice(8 + char_shift, 10 + char_shift);
  const time = timePart.slice(0, 5);
  return `${day}.${month}.${year} ${time}`;
}

const mapStateToProps = (state: State) => ({
  title: state.chat.title,
  messages: state.chat.messages,
});

export default connect(mapStateToProps)(Header);
