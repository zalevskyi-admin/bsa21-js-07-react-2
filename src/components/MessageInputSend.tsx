import React from 'react';
import '../styles/MessageInput.css';

export class MessageInputSend extends React.Component<{ send: Function }> {
  state: {
    text: string;
  };
  constructor(props: { send: Function }) {
    super(props);
    this.state = { text: '' };
  }
  render() {
    return (
      <div className='message-input'>
        <textarea
          className='message-input-text'
          value={this.state.text}
          onChange={e => this.setText(e.target.value)}></textarea>
        <button
          className='message-input-button'
          onClick={e => {
            this.props.send(this.state.text);
            this.setText('');
          }}>
          Send
        </button>
      </div>
    );
  }
  setText(text: string) {
    this.setState({ text: text });
  }
}
