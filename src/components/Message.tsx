import React from 'react';
import '../styles/Message.css';
import { BasicMessage } from './BasicMessage';
import { Message as MessageData } from '../rootReducer';

export class Message extends React.Component<{
  message: MessageData;
  toggleLike: Function;
}> {
  render() {
    return (
      <div className='message'>
        <Avatar avatar={this.props.message.avatar} />
        <User user={this.props.message.user} />
        <BasicMessage
          text={this.props.message.text}
          time={
            this.props.message.editedAt
              ? getTime(this.props.message.editedAt)
              : getTime(this.props.message.createdAt)
          }
        />
        <Like
          id={this.props.message.id}
          liked={this.props.message.liked}
          toggleLike={this.props.toggleLike}
        />
      </div>
    );
  }
}

class Avatar extends React.Component<{ avatar: string }> {
  render() {
    return (
      <img
        className='message-user-avatar'
        src={this.props.avatar}
        alt='User avatar'
      />
    );
  }
}

class User extends React.Component<{ user: string }> {
  render() {
    return <div className='message-user-name'>{this.props.user}</div>;
  }
}

class Like extends React.Component<{
  id: string;
  liked?: boolean;
  toggleLike: Function;
}> {
  constructor(props: { id: string; liked?: boolean; toggleLike: Function }) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }
  render() {
    return (
      <input
        className={this.props.liked ? 'message-liked' : 'message-like'}
        type='checkbox'
        checked={this.props.liked || false}
        onChange={this.onChange}
      />
    );
  }
  onChange() {
    this.props.toggleLike(this.props.id);
  }
}

function getTime(isoDate: string) {
  return new Date(isoDate).toTimeString().slice(0, 5);
}
