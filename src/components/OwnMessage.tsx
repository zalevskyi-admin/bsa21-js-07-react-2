import React from 'react';
import '../styles/OwnMessage.css';
import { BasicMessage } from './BasicMessage';
import { Message as MessageData } from '../rootReducer';

export class OwnMessage extends React.Component<{
  message: MessageData;
  edit: Function;
  delete: Function;
}> {
  render() {
    return (
      <div className='own-message'>
        <BasicMessage
          text={this.props.message.text}
          time={
            this.props.message.editedAt
              ? getTime(this.props.message.editedAt)
              : getTime(this.props.message.createdAt)
          }
        />
        <Button
          id={this.props.message.id}
          onClick={this.props.edit}
          name='Edit'
          className='message-edit'
        />
        <Button
          id={this.props.message.id}
          onClick={this.props.delete}
          name='Delete'
          className='message-delete'
        />
      </div>
    );
  }
}

class Button extends React.Component<{
  id: string;
  onClick: Function;
  name: string;
  className: string;
}> {
  render() {
    return (
      <button
        type='button'
        className={this.props.className}
        onClick={e => {
          this.props.onClick(this.props.id);
        }}>
        {this.props.name}
      </button>
    );
  }
}

function getTime(isoDate: string) {
  return new Date(isoDate).toTimeString().slice(0, 5);
}
