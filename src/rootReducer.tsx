export interface State {
  chat: Chat;
}

export interface Chat {
  messages: Message[];
  editModal: boolean;
  preloader: boolean;
  loadError: string | null;
  title: string;
  currentUserId: string;
  currentUserName: string;
  messageToEditId: string | null;
  messageToEditText: string | null;
}

export interface Message {
  avatar: string;
  createdAt: string;
  editedAt: string;
  id: string;
  text: string;
  user: string;
  userId: string;
  liked?: boolean;
}

export type Action =
  | { type: ActionType.loadStart }
  | {
      type: ActionType.loadMessages;
      payload: { messages: Message[] };
    }
  | { type: ActionType.setLoadError; payload: { error: string } }
  | { type: ActionType.addMessage; payload: { text: string } }
  | { type: ActionType.deleteMessage; payload: { id: string } }
  | { type: ActionType.setMessageEdit; payload: { id: string } }
  | { type: ActionType.cancelMessageEdit }
  | { type: ActionType.updateText; payload: { id: string; text: string } }
  | { type: ActionType.toggleLike; payload: { id: string } };

export enum ActionType {
  loadStart,
  loadMessages,
  setLoadError,
  addMessage,
  deleteMessage,
  setMessageEdit,
  cancelMessageEdit,
  updateText,
  toggleLike,
  togglePreloader,
}

const initialState: State = {
  chat: {
    messages: [],
    editModal: false,
    preloader: true,
    loadError: null,
    title: 'Finance chat',
    currentUserId: 'me',
    currentUserName: 'me',
    messageToEditId: null,
    messageToEditText: null,
  },
};

export default function rootReducer(
  state: State = initialState,
  action: Action,
): State {
  switch (action.type) {
    case ActionType.loadStart:
      return loadStart(state);
    case ActionType.loadMessages:
      return loadMessages(state, action.payload.messages);
    case ActionType.setLoadError:
      return setLoadError(state, action.payload.error);
    case ActionType.addMessage:
      return addMessage(state, action.payload.text);
    case ActionType.deleteMessage:
      return deleteMessage(state, action.payload.id);
    case ActionType.setMessageEdit:
      return setMessageEdit(state, action.payload.id);
    case ActionType.cancelMessageEdit:
      return cancelMessageEdit(state);
    case ActionType.updateText:
      return updateText(state, action.payload.id, action.payload.text);
    case ActionType.toggleLike:
      return toggleLike(state, action.payload.id);
    default:
      return state;
  }
}

function copyState(state: State): State {
  return {
    ...state,
    chat: {
      ...state.chat,
      messages: state.chat.messages.map(message => {
        return { ...message };
      }),
    },
  };
}

function loadStart(state: State): State {
  const newState = copyState(state);
  newState.chat = {
    ...newState.chat,
    preloader: true,
    loadError: null,
    editModal: false,
  };
  return newState;
}

function loadMessages(state: State, messages: Message[]): State {
  const newState = copyState(state);
  newState.chat = {
    ...newState.chat,
    preloader: false,
    loadError: null,
    editModal: false,
    messages: [...newState.chat.messages, ...messages],
  };
  return newState;
}

function setLoadError(state: State, error: string): State {
  const newState = copyState(state);
  newState.chat = {
    ...newState.chat,
    preloader: false,
    loadError: error,
    editModal: false,
  };
  return newState;
}

function addMessage(state: State, text: string): State {
  const newState = copyState(state);
  const now = new Date();
  const message = {
    avatar: '',
    createdAt: now.toISOString(),
    editedAt: '',
    id: `${newState.chat.currentUserId}-${now.toISOString()}`,
    text: text,
    user: newState.chat.currentUserName,
    userId: newState.chat.currentUserId,
  };
  newState.chat.messages.push(message);
  return newState;
}

function deleteMessage(state: State, id: string): State {
  const newState = copyState(state);
  newState.chat.messages = newState.chat.messages.filter(
    message => message.id !== id,
  );
  return newState;
}

function setMessageEdit(state: State, id: string): State {
  const newState = copyState(state);
  newState.chat.messageToEditId = id;
  newState.chat.messages.forEach(message => {
    if (message.id === id) {
      newState.chat.messageToEditText = message.text;
    }
  });
  return newState;
}

function cancelMessageEdit(state: State): State {
  const newState = copyState(state);
  newState.chat.messageToEditId = null;
  newState.chat.messageToEditText = null;
  return newState;
}

function updateText(state: State, id: string, text: string): State {
  const newState = copyState(state);
  newState.chat.messages = newState.chat.messages.map(message => {
    if (message.id === id) {
      message.text = text;
    }
    return message;
  });
  newState.chat.messageToEditId = null;
  newState.chat.messageToEditText = null;
  return newState;
}

function toggleLike(state: State, id: string): State {
  const newState = copyState(state);
  newState.chat.messages = newState.chat.messages.map(message => {
    if (message.id === id) {
      message.liked = !message.liked;
    }
    return message;
  });
  console.log(newState);
  return newState;
}
